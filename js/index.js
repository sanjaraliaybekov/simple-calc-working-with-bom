$('#myCarousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    dots: true,

    autoplay: true,
    autoplayTimeout: 1000,
    autoplayHoverPause: true,

    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 2
        }
    }
});
$('#myCarousel1').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    dots: true,

    autoplay: true,
    autoplayTimeout: 1000,
    autoplayHoverPause: true,

    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 4
        },
        1000: {
            items: 4
        }
    }
});